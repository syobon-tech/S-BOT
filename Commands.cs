using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

namespace SBOT
{
    class BasicCommands
    {
        [Command("repeatme")]
        [Aliases("repeat")]
        [Description("任意のテキストを送信します。")]
        public async Task Repeatme(CommandContext ctx, [Description("送信する文字列")] params String[] text)
        {
            await ctx.RespondAsync(String.Join(" ", text));
        }

        [Command("say")]
        [Description("BOTが発言します。")]
        public async Task Say(CommandContext ctx, [Description("送信する文字列")] params String[] text)
        {
            await ctx.RespondAsync(String.Join(" ", text));
            await ctx.Message.DeleteAsync();
        }

        [Command("dm")]
        [Description("BOTがあなたにDMしてきます。")]
        public async Task Dm(CommandContext ctx, [Description("送信する文字列")] params String[] text)
        {
            await ctx.Member.SendMessageAsync(String.Join(" ", text));
        }

        [Command("random")]
        [Description("任意の範囲の乱数を返します")]
        public async Task Random(CommandContext ctx, [Description("最小値")] int min = 0, [Description("最大値")] int max = 100)
        {
            string result = Core.rnd.Next(min, max + 1).ToString();
            await ctx.RespondAsync(result);
        }

        [Command("rank")]
        [Aliases("exp")]
        [Description("経験値量を表示します")]
        public async Task Rank(CommandContext ctx, [Description("ユーザー")] string user = "")
        {
            var exps = Core.exps;

            string name = "";
            ulong id = 0;
            int exp = 0;

            if (user == "")
            {
                exps.TryGetValue(ctx.Member.Id.ToString(), out exp);
                id = ctx.Member.Id;
                name = ctx.Member.DisplayName;

            }
            else if (ulong.TryParse(user, out id))
            {
                exps.TryGetValue(user, out exp);
                name = ctx.Guild.GetMemberAsync(id).Result.DisplayName;

            }
            int rank = 0;
            foreach (var item in exps)
            {
                rank++;
                if (item.Key == id.ToString())
                {
                    break;
                }
            }
            int level = (int)Math.Floor(Math.Sqrt(exp / 50));
            int currentLevelExp = (int)Math.Pow(level, 2) * 50;
            int nextLevelExp = (int)Math.Pow((level + 1), 2) * 50;
            int deltaExp = nextLevelExp - currentLevelExp;
            int currentExp = exp - currentLevelExp;

            int progress = (int)Math.Round((double)currentExp / deltaExp * 20);
            string progressBar = "";
            if (progress > 1) {
                progressBar = new String('=', progress - 1);
                progressBar += ">";
            }
            while (progressBar.Length <= 20)
            {
                progressBar += " ";
            }

            await ctx.RespondAsync($"{name} :\rRank: {rank}\rLevel: {level}\rExp: {exp} (Lv.{level} + {currentExp} / {deltaExp})\r次のレベルまで: {nextLevelExp - exp}\r`{currentLevelExp}[{progressBar}]{nextLevelExp}`");
        }

        [Command("levels")]
        [Aliases("level", "ranking")]
        [Description("経験値ランキングを表示します")]
        public async Task Levels(CommandContext ctx, [Description("ページ")] int page = 1)
        {
            var exps = Core.exps;
            int rank = 0;
            string itemName;
            string ranking = "```\r";

            foreach (var item in exps)
            {
                rank++;
                if (rank <= 10 * (page - 1))
                {
                    continue;
                }
                try
                {
                    itemName = ctx.Guild.GetMemberAsync(ulong.Parse(item.Key)).Result.DisplayName;
                }
                catch
                {
                    itemName = "不明なユーザー";
                }
                ranking += $"{rank}. {itemName}\r     Total Exp: {item.Value}\r\r";
                if (rank >= 10 * page)
                {
                    break;
                }
            }
            ranking += $"Pages: {page} / {Math.Ceiling((decimal)exps.Count / 10)}";
            ranking += "```";
            await ctx.RespondAsync(ranking);
        }

        [Command("dice")]
        [Aliases("throw")]
        public async Task ThrowDice(CommandContext ctx, [Description("面数")] int face = 6, [Description("ダイス数")] int throwCount = 1)
        {
            if (face < 2 || throwCount < 1)
            {
                await ctx.RespondAsync("引数が不正です。");

            }
            else
            {
                ulong summary = 0;
                for (int i = 1; i <= throwCount; i++)
                {
                    int result = Core.rnd.Next(1, face + 1);
                    summary += (ulong)result;
                }
                await ctx.RespondAsync($"{face}面ダイスを{throwCount}回振ると…\r合計は{summary}　平均は{(summary / (double)throwCount).ToString("F")}でした。");
            }
        }

        [Command("roll")]
        public async Task RollDice(CommandContext ctx, [Description("ダイス")] string dice = "1d100")
        {
            if(dice.Contains('d'))
            {
                ulong summary = 0;
                string summaryText = "";
                string diceTypeText = "";
                if(dice.Contains('+'))
                {
                    string[] plusNum = dice.Split('+');
                    summary += ulong.Parse(plusNum[1]);
                    diceTypeText = plusNum[0];
                }
                else
                {
                    diceTypeText = dice;
                }
                string[] diceType = diceTypeText.Split('d');
                int face = int.Parse(diceType[1]);
                int throwCount = int.Parse(diceType[0]);

                if (face < 2 || throwCount < 1)
                {
                    await ctx.RespondAsync("引数が不正です。");
                }
                else
                {
                    for (int i = 1; i < throwCount; i++)
                    {
                        int result = Core.rnd.Next(1, face + 1);
                        summary += (ulong)result;
                        summaryText += $"{result}, ";
                    }
                    int resultLast = Core.rnd.Next(1, face + 1);
                    summary += (ulong)resultLast;
                    summaryText += $"{resultLast}";
                    if (throwCount == 1)
                    {
                        await ctx.RespondAsync($"{face}面ダイスを振ると…\r{summary}！");
                    }
                    else if (throwCount <= 20)
                    {
                        await ctx.RespondAsync($"{face}面ダイスを{throwCount}回振ると…\r{summaryText}\r合計 : {summary}");
                    }
                    else
                    {
                        await ctx.RespondAsync($"{face}面ダイスを{throwCount}回振ると…\r合計は{summary}　平均は{(summary / (double)throwCount).ToString("F")}でした。");
                    }
                }
            }
            else
            {
                string message = "";
                int threshold = int.Parse(dice);
                int result = Core.rnd.Next(1,101);

                message += $"{result}　";
                if (result <= threshold)
                {
                    if (result <= 5)
                    {
                        message += "クリティカル！";
                    }
                    else
                    {
                        message += "成功";
                    }
                }
                else
                {
                    if (result > 95)
                    {
                        message += "ファンブル！";
                    }
                    else
                    {
                        message += "失敗";
                    }
                }

                await ctx.RespondAsync(message);
            }
        }

        [Command("ojichat")]
        [Description("BOTがおじさんになります。")]
        public async Task Ojichat(CommandContext ctx, [Description("名前")] string name = "")
        {
            Process p = Process.Start("chmod", "+x /app/ojichat");
            p.WaitForExit();

            ProcessStartInfo psInfo = new ProcessStartInfo();
            psInfo.FileName = "ojichat";
            if (name != "")
            {
                psInfo.Arguments = $"\"{name}\"";
            }
            else
            {
                psInfo.Arguments = $"\"{ctx.Member.DisplayName}\"";
            }
            psInfo.CreateNoWindow = true;
            psInfo.UseShellExecute = false;
            psInfo.RedirectStandardOutput = true;
            Process q = Process.Start(psInfo);

            string output = q.StandardOutput.ReadToEnd();
            output = output.Replace("\r\r\n", "\n");

            await ctx.RespondAsync(output);
        }

        [Command("countrygacha")]
        [Cooldown(1, 10, CooldownBucketType.User)]
        [Description("国ガチャを引けます")]
        public async Task CountryGacha(CommandContext ctx, string mode = "")
        {
            var items = new List<Object[]>();
            string[] grades = { "N", "UN", "R", "SR", "SSR", "UR" };
            long max = 0;
            using (var sr = new StreamReader("/app/countryGacha.csv"))
            {
                while (sr.Peek() != -1)
                {
                    var item = sr.ReadLine().Split(',');
                    items.Add(new Object[] { int.Parse(item[0]), item[1], long.Parse(item[2]) });
                    max += int.Parse(item[2]);
                }
            }
            if (mode == "list")
            {
                string output = "";
                int grade = 0;
                foreach (var i in items)
                {
                    if ((int)i[0] > grade)
                    {
                        output += $"{grades[(int)i[0]]}\r";
                        grade++;
                    }
                    double percentage = (long)i[2] / max;
                    output += $"{(string)i[1]}{string.Format("{0, 5}", percentage.ToString())}\r";
                }
                await ctx.RespondAsync(output);
            }
            else
            {
                string output = $"{ctx.Member.Mention}\r";
                var rand = new Random();
                long num = long.MaxValue;
                if (max < int.MaxValue)
                {
                    num = rand.Next((int)max) + 1;
                }
                else
                {
                    while (num > max)
                    {
                        int numoku = rand.Next((int)(max / 1000000000) + 1);
                        int numichi = rand.Next(1000000000);
                        num = (long)numoku * (long)1000000000 + (long)numichi + (long)1;
                    }
                }
                long sum = 0;
                foreach (var i in items)
                {
                    sum += (long)i[2];
                    if (num <= sum)
                    {
                        string grade = grades[(int)i[0]];
                        string exclamations = new string('!', (int)i[0] + 1);
                        output += $"{grade}{exclamations}\r{(string)i[1]}(人口約{((long)i[2] * 100).ToString("N0")}人)を引きました{exclamations}";
                        break;
                    }
                }
                await ctx.RespondAsync(output);
            }
        }

        [Command("gacha")]
        [Cooldown(1, 10, CooldownBucketType.User)]
        [Description("空気ガチャを引けます")]
        public async Task Gacha(CommandContext ctx, string mode = "")
        {
            if (mode == "list")
            {
                await ctx.RespondAsync("```\rN\r　窒素(N)         　78.074562245 (調整値)\r\rUN\r　酸素(O₂)          20.9476%\r\rR\r　アルゴン(Ar)　     0.934%\r\rSR\r　二酸化炭素(CO₂)　  0.041%\r　ネオン(Ne)       0.001818%\r　ヘリウム(He)　     0.000524%\r　メタン(CH₄)　      0.000181%\r　クリプトン(Kr)　   0.000114%\r　二酸化硫黄(SO₂)　  0.0001%\r\rSSR\r　水素(H₂)　         0.00005%\r　一酸化二窒素(N₂O)　0.000032%\r　キセノン(Xe)　     0.0000087%\r　オゾン(O₃)　       0.000007%\r　二酸化窒素(NO₂)　  0.000002%\r　ヨウ素(I₂)　       0.000001%\r\rUR\r　クロロメタン(CH₃Cl)0.000000055%\r```");
            }
            else
            {
                string output = $"{ctx.Member.Mention}";
                if (mode == "10")
                {
                    await ctx.RespondAsync(output);
                    for(int i = 0; i < 10; i++)
                    {
                        await ctx.RespondAsync(AirGacha());
                    }
                }
                else
                {
                    output += "\r";
                    output += AirGacha();
                    await ctx.RespondAsync(output);
                }
            }
        }

        public string AirGacha()
        {
            string output = "";
            var rand = new Random();
            int numoku = rand.Next(100);
            int numichi = rand.Next(1000000000);
            long num = (long)numoku * (long)1000000000 + (long)numichi + (long)1;
            if (num <= 55)
            {
                output += "UR!!!!!!\rクロロメタン(CH₃Cl)を引きました!!!!!!";
            }
            else if (num <= 1055)
            {
                output += "SSR!!!!!\rヨウ素(I₂)を引きました!!!!!";
            }
            else if (num <= 3055)
            {
                output += "SSR!!!!!\r二酸化窒素(NO₂)を引きました!!!!!";
            }
            else if (num <= 10055)
            {
                output += "SSR!!!!!\rオゾン(O₃)を引きました!!!!!";
            }
            else if (num <= 18755)
            {
                output += "SSR!!!!!\rキセノン(Xe)を引きました!!!!!";
            }
            else if (num <= 50755)
            {
                output += "SSR!!!!!\r一酸化二窒素(N₂O)を引きました!!!!!";
            }
            else if (num <= 100755)
            {
                output += "SSR!!!!!\r水素(H₂)を引きました!!!!!";
            }
            else if (num <= 200755)
            {
                output += "SR!!!!\r二酸化硫黄(SO₂)を引きました!!!!";
            }
            else if (num <= 314755)
            {
                output += "SR!!!!\rクリプトン(Kr)を引きました!!!!";
            }
            else if (num <= 495755)
            {
                output += "SR!!!!\rメタン(CH₄)を引きました!!!!";
            }
            else if (num <= 1019755)
            {
                output += "SR!!!!\rヘリウム(He)を引きました!!!!";
            }
            else if (num <= 2837755)
            {
                output += "SR!!!!\rネオン(Ne)を引きました!!!!";
            }
            else if (num <= 43837755)
            {
                output += "SR!!!!\r二酸化炭素(CO₂)を引きました!!!!";
            }
            else if (num <= 977837755)
            {
                output += "R!!!\rアルゴン(Ar)を引きました!!!";
            }
            else if (num <= 21925437755)
            {
                output += "UN!!\r酸素(O₂)を引きました!!";
            }
            else
            {
                output += "N!\r窒素(N₂)を引きました!";
            }
            return output;
        }

        public static List<DiscordChannel> channels = new List<DiscordChannel>();
        public static bool mention;

        [Command("add_channel_shingekin")]
        [Description("ﾌﾌﾌ")]
        public async Task AddShingekinChannel(CommandContext ctx)
        {
            channels.Add(ctx.Channel);
        }

        [Command("start_mention_shingekin")]
        [Hidden]
        public async Task MentionShingekin(CommandContext ctx)
        {
            if (!mention)
            {
                mention = true;
                while (channels.Count > 0)
                {
                    for (int i = 0; i < channels.Count; i++)
                    {
                        var message = await channels[i].SendMessageAsync("<@478881118968610816>");
                        await message.DeleteAsync();
                    }
                    await Task.Delay(60000);
                }
                mention = false;
            }
            else
            {
                await ctx.RespondAsync("既に実行中です。");
            }
        }

        [Command("stop_mention_shingekin")]
        [Description("ストーップ")]
        public async Task StopMention(CommandContext ctx)
        {
            channels.Remove(ctx.Channel);
            await ctx.RespondAsync("OK.");
        }

        [Command("countdown")]
        [Cooldown(1, 86400, CooldownBucketType.Guild)]
        [Description("年越しカウントダウン！")]
        public async Task Countdown(CommandContext ctx)
        {
            DateTimeOffset _now = DateTimeOffset.UtcNow;
            int nowYear = _now.Year;
            int nextYear = _now.Year + 1;
            DateTime yearChangedt = new DateTime(nextYear, 1, 1);
            DateTimeOffset yearChange = new DateTimeOffset(nextYear, 1, 1, 0, 0, 0, new TimeSpan(9, 0, 0));

            while (true)
            {
                DateTimeOffset now = DateTimeOffset.UtcNow;
                nowYear = now.Year;
                TimeSpan diff = yearChange - now;
                int diffSeconds = (int)Math.Floor(diff.TotalSeconds);

                if (diffSeconds <= 0)
                {
                    await ctx.RespondAsync("@everyone\rHappy New Year !!!");
                    break;
                }

                if (diffSeconds <= 60)
                {
                    await ctx.RespondAsync($"{nextYear}年まであと{diffSeconds}秒");
                    await Task.Delay(1000);
                }
                else if (now.Second == 0)
                {
                    int diffMinutes = (int)Math.Round(diffSeconds / 60.0);
                    await ctx.RespondAsync($"{nextYear}年まであと{diffMinutes}分");
                    await Task.Delay(1000);
                }
            }
        }
    }
}
