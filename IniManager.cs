using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SBOT {
    class IniManager {

        /// <summary>
        /// iniファイルを読み込む
        /// </summary>
        /// <param name="filename">ファイル名</param>
        /// <param name="encode">ファイルのエンコード</param>
        /// <returns></returns>
        public static Dictionary<string, int> Load( string filename, Encoding encode = null )
        {
            // ファイルのエンコードを決定する
            if ( encode == null) {
                encode = new UTF8Encoding( false );
            }

            var datas = new Dictionary<string, int>();

            var lines = File.ReadAllLines( filename, encode );
            foreach ( var line in lines ) {
                // keyとvalueの区切り目を見つける
                var pos = line.IndexOf( '=' );
                if ( pos < 0 ) {
                    continue;
                }

                // key-valueを一覧に追加
                var key = line.Substring( 0, pos );
                var val = int.Parse(line.Substring( pos + 1 ));
                datas[key] = val;
            }

            // 追加された一覧を返す
            return datas;
        }


        /// <summary>
        /// iniファイルに保存する
        /// </summary>
        /// <param name="filename">ファイル名</param>
        /// <param name="datas">保存するデータ</param>
        /// <param name="encode">ファイルのエンコード</param>
        public static void Save( string filename, Dictionary<string, int> datas, Encoding encode =   null )
        {
            // ファイルのエンコードを決定する
            if ( encode == null ) {
                encode = new UTF8Encoding( false );
            }

            using ( StreamWriter writer = new StreamWriter( filename, false, encode ) ) {

                foreach ( string key in datas.Keys ) {
                    // keyに"="が入っていたら例外を投げる
                    if ( key.IndexOf( "=" ) > 0 ) {
                        throw new ArgumentException( @"invalid key[{key}]" );
                    }

                    var value = datas[key];
                    writer.WriteLine( key + "=" + value.ToString() );
                }
            }
        }
    }
}